import csv
import matplotlib.pyplot as plt
from statistics import mean
import numpy as np

# lists to store the dynamic data from the dataset
# in the lists it includes the music from 2000-2058 which have different shapes we need to process the music to
# leave those out because they result in different shapes

# arousal = open('..\\MUSIC DEAM\\annotations\\annotations averaged per song\\'
#                'dynamic (per second annotations)\\arousal.csv')
# csv_arousal = csv.reader(arousal)
# valence = open('..\\MUSIC DEAM\\annotations\\annotations averaged per song\\'
#                'dynamic (per second annotations)\\valence.csv')
# csv_valence = csv.reader(valence)


def full_info(file):
    #  row 0 is all the titles
    #  row 1 to 1743 inclusive corressponds to song_id 2 and 1999
    #  row 1744 inclusive to 1802 is song_id 2000 and 2058
    ind_song = []
    full_ls = []
    for row in file:
        if row[0] == 'song_id':
            print(row)  # Prints the column titles
        elif row[0] == "2000":
            break
        else:
            for i in range(len(row)):
                ind_song.append(float(row[i]))  # For each song, there are 60 ratings
            full_ls.append(ind_song)
            ind_song = []
    return full_ls


'''
Create a list with the 0.5s time intercals from 0s to 60s 
'''


def time_ls():
    new_list = []
    for i in range(0, 30):
        new_list.append(i)
        new_list.append(i+0.5)
    return new_list

# https://pythonprogramming.net/how-to-program-best-fit-line-machine-learning-tutorial/


def best_fit_slope_and_intercept(ls):
    times = time_ls()
    xs = np.array(times, dtype=np.float64)
    ls_full = []
    song_grad_intercept = []
    for i in ls:
        ls1 = []
        indiv_list = []
        for j in range(1, len(i)):
            ls1.append(i[j])  # Create a list of just the V or A values since the first index is the id
        # print(len(ls1))
        ys = np.array(ls1, dtype=np.float64)  # convert to an np.array to perform statistical functions
        # print(np.shape(ys))
        grad = (((mean(xs)*mean(ys)) - mean(xs*ys)) / ((mean(xs)*mean(xs)) - mean(xs*xs)))
        intercept = mean(ys) - grad * mean(xs)
        indiv_list.append(i[0])  # first index is the song ID
        indiv_list.append(grad)  # Second index is the gradient of the line
        indiv_list.append(intercept)  # Third index is the intercept of the line
        # song_grad_intercept.append(indiv_list)#  Create a list just to store the ID, gradient and the intercept
        for j in range(1, len(i)):
            indiv_list.append(i[j])  # format of ls
        ls_full.append(indiv_list)  # format of the list [song_id, grad, intercept, value1, ... value60]

    return ls_full  # , song_grad_intercept


def squared_error(ys_orig, ys_line):  # Note that the x value is the time scale. This function has to be run for V & A
    return sum((ys_line - ys_orig)**2)


def coefficient_of_determination(all_songs):  # Takes in the full list of all the songs with its elements of indiv_songs
    full_ls = []

    times = time_ls()
    xs = np.array(times, dtype=np.float64)
    for indiv_songs in all_songs:
        indiv_list = []
        #  song_id = indiv_songs[0]
        m = indiv_songs[1]
        b = indiv_songs[2]
        for num in range(3):
            indiv_list.append(round(indiv_songs[num], 3))  # create a list with song_id, Gradient, intercept as 1st 3
        ys_line_norm = [(m * x) + b for x in xs]  # Create a list of the predicted Y values based on the line
        ys_line = np.array(ys_line_norm, dtype=np.float64)
        ys_orig_normal = []  # initialise a list that will take in the original Y values that made the reg line
        for i in range(3, len(indiv_songs)):
            ys_orig_normal.append(indiv_songs[i])
        ys_orig = np.array(ys_orig_normal, dtype=np.float64)
        # For every y point, at the mean of the whole line to the list.
        # Just to get the number of means to correspond to the number of ys
        y_mean_line1 = [mean(ys_orig) for y in ys_orig]
        y_mean_line = np.array(y_mean_line1, dtype=np.float64)
        squared_error_regr = squared_error(ys_orig, ys_line)
        squared_error_y_mean = squared_error(ys_orig, y_mean_line)
        r_val = round(1 - (squared_error_regr/squared_error_y_mean), 3)
        indiv_list.append(r_val)  # The list is now ID, Grad, intercept, rval
        # for i in range(3, len(indiv_songs)):
        #     indiv_list.append(indiv_songs[i])  # add the y values after id, grad, intercept, r_val
        full_ls.append(indiv_list)
    return full_ls


def filter_by_r(val_r_ls, arou_r_ls, threshold_r = 0.9):
    full_ls = []
    for indeces in range(len(val_r_ls)):
        indiv_ls = []
        if val_r_ls[indeces][3] > threshold_r and arou_r_ls[indeces][3] > threshold_r:
            for num_val in range(3):  #Change the number to 4 to include r value
                indiv_ls.append(val_r_ls[indeces][num_val])
            for num_arou in range(1, 3):  # change the number to 4 to include r value
                indiv_ls.append(arou_r_ls[indeces][num_arou])
            full_ls.append(indiv_ls)
    return full_ls  # this output list are in the form [[songID, gradV, interV, gradA, interA]...]


def filter_by_grad(post_r, thresh=0.1):
    filtered_grad_ls = []
    for indiv_song in post_r:
        if indiv_song[1] <= thresh and indiv_song[3] <= thresh:
            filtered_grad_ls.append(indiv_song)
    return filtered_grad_ls


def plot_val_arou(valence_list, arousal_list, song_id):
    time_list = time_ls()
    for ind, val in enumerate(valence_list):
        if val[0] == song_id:
            plt.plot(time_list, val[1:], 'bo')
            plt.plot(time_list, arousal_list[ind][1:], 'ro')
    #plt.axis(0, 31, -1, 1)
    plt.xlabel('time/seconds')
    plt.ylabel('Valence (blue)/Arousal (red)')
    plt.title('song ID:' + str(song_id) + ' val/arousal against time')
    plt.axis([0,35,-1,1])
    plt.show()


# ls_arousal = full_info(csv_arousal)
# ls_valence = full_info(csv_valence)
# # print(len(ls_arousal))  # 1802
# # print(len(ls_arousal[0]))  # print the length of the first list. Output: 61 since it includes song_id
# # print(ls_arousal[0]) # print the first list
# #  print(len(ls_valence))  # 1802
# # print(ls_valence)
# arousal.close()
# valence.close()
#
# '''
# plot the graph of valence/arousal against time
# '''
# plot_val_arou(ls_valence,ls_arousal, 178)

'''
computing the gradient and the intercept for each list
'''
# ls_arousal_full = best_fit_slope_and_intercept(ls_arousal)
# ls_valence_full = best_fit_slope_and_intercept(ls_valence)
# # print(ls_arousal_full)
# # print(ls_valence_full)

'''
computing the r^2 value
'''
# ls_arousal_scored = coefficient_of_determination(ls_arousal_full)
# #  print(ls_arousal_scored[0])
# #  print(ls_arousal_scored[1])
# ls_valence_scored = coefficient_of_determination(ls_valence_full)
# #  print(ls_valence_scored[0])


'''
filter out the values that dont meet the r requirement and print the length of the resulting list  
'''
# filtered_ls = filter_by_r(ls_arousal_scored, ls_arousal_scored, 0.9)
# print(filtered_ls)
# print(len(filtered_ls))
'''
This function has been run to save the values 
'''
# with open("valence_graded.csv", "w") as fb:
#     a = csv.writer(fb)
#     a.writerows(ls_valence_scored)
#
# with open("arousal_graded.csv", "w") as f:
#     b = csv.writer(f)
#     b.writerows(ls_arousal_scored)
