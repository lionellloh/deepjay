import csv
from math import sqrt
from pydub import AudioSegment
import step_video
import equations_music
import scene_detection
import moviepy.editor as mp

import sys

'''
Change the file paths below 
'''

filename = sys.argv[1].split(".")[0]
movie_to_overlay_music = sys.argv[1] #'256.mp4'  # This is the movie file to overlay the music on
combined_audio_file = "{}.mp3".format(filename)
video_output_file = "{}_output.mov".format(filename)

file_path_video = "{}.csv".format(filename)  # csv files containing the VA for the video
file_path_music_arousal = 'arousal.csv'  # This is the path to the arousal set
file_path_music_valence = 'valence.csv'  # This is the path to the valence set
file_path_scene = "{}-Scenes.csv".format(filename)  # This is the link to the csv containing the scenes from PySceneDetect
file_path_music = 'MEMD_audio/'  # This is the path to all the musics

'''
end of the file paths 
'''

emotion = open(file_path_video)
csv_emotion = csv.reader(emotion)  # note that for gladiator, there are 1802 data points

arousal = open(file_path_music_arousal)
csv_arousal = csv.reader(arousal)

valence = open(file_path_music_valence)
csv_valence = csv.reader(valence)

scene = open(file_path_scene)
csv_scenes = csv.reader(scene)

combined_music = AudioSegment.from_mp3('blank.mp3')
seconds_to_ms = 1000


'''
The functions below are for the matching part 
'''
def euclid_dist(desired_val, desired_arousal, actual_val, actual_arousal):
    dist1 = sqrt((desired_val - actual_val) ** 2 + (desired_arousal - actual_arousal) ** 2)
    return dist1


def match_vid_music(video_steps, music_eqn):
    # video list is [[0,t1,v1,a1],[t1+1,t2,v2,a2]...]
    # music list is [[id1, gv1, iv1, ga1, ia1],...]
    euc_dist_ls = []
    matched_ls = [[0, 0, 0, 0]]  # Create a matched list just for the program to run
    for cluster_steps in video_steps:
        start_time = cluster_steps[0]
        end_time = cluster_steps[1] + 1
        total_time = end_time - start_time
        chosen = [start_time]
        chosen.append(end_time)
        chosen.append(total_time)
        val_value = cluster_steps[2]
        arou_val = cluster_steps[3]
        first_music_v = music_eqn[0][2]
        first_music_a = music_eqn[0][4]
        dist = euclid_dist(val_value, arou_val, first_music_v, first_music_a)
        chosen.append(music_eqn[0][0])
        for analyse_music in range(1, len(music_eqn)):
            dist1 = euclid_dist(val_value, arou_val, music_eqn[analyse_music][2], music_eqn[analyse_music][4])
            if dist1 < dist and music_eqn[analyse_music][0] != matched_ls[-1][3]:
                dist = dist1
                chosen[3] = music_eqn[analyse_music][0]
        euc_dist_ls.append(round(dist,3))
        matched_ls.append(chosen)
    matched_ls.pop(0)
    print(euc_dist_ls)
    return matched_ls  # [[t1,t2,tt,ID], [t2,t3,tt,ID],...,]


def music_start_end(match_list, scene_list):
    # format of match_list [[t1,t2,tt,ID], [t2,t3,tt,ID],...,]
    # format of scene_list [t1,t2,t3,t4,t5...]
    scene_va_list = []
    for indiv_match in range(len(match_list)):
        indiv_func = []
        indiv_func.append(match_list[indiv_match][3])  # Appends the song ID
        starting_time = match_list[indiv_match][0]  # This is the starting time of each match
        ending_time = match_list[indiv_match][1]  # This is the ending time of each match
        if starting_time == 0:  # mainly for the fist song since the fist song is directly matched
            indiv_func.append(starting_time)  # Appends the starting value which is just 0
            time_dist = abs(ending_time - scene_list[0])  # use the first time as the reference time
            for index_scene, indiv_scene in enumerate(scene_list):
                time_dist1 = abs(ending_time - indiv_scene)
                if time_dist1 < time_dist:
                    time_dist = time_dist1
                    new_end = indiv_scene
            indiv_func.append(new_end)
        elif indiv_match == len(match_list) - 1:  # this is for the last entry that we dont need to dist last value
            starting_time = scene_va_list[-1][2]
            indiv_func.append(starting_time)
            indiv_func.append(ending_time)
        else:
            starting_time = scene_va_list[-1][2]
            time_dist = abs(ending_time - scene_list[0])  # use the first time as the reference time
            for index_scene, indiv_scene in enumerate(scene_list):
                time_dist1 = abs(ending_time - indiv_scene)
                if time_dist1 < time_dist and indiv_scene != starting_time:  # may want to change this line to make sure that the new end time is not same as start time
                    time_dist = time_dist1
                    new_end = indiv_scene
            indiv_func.append(starting_time)
            indiv_func.append(new_end)
        scene_va_list.append(indiv_func)
    return scene_va_list  # [[ID, music_start_last_inst, music_end_last_inst], ...]


def discover_instances(music_time_start, music_time_end):
    music_len = 30
    music_time = round(music_time_end - music_time_start, 2)
    num_instances = int(music_time//music_len) + 1  # for 1 instance, it will say 0
    remaining_time = round(music_time % music_len,2)
    return [music_time, num_instances, remaining_time]


def setting_the_music(scene_va_list): # [[ID, music_start_last_inst, music_end_last_inst], ...]
    functional_ls = []
    for indiv_scenes in scene_va_list:
        indiv_func_ls = []
        indiv_func_ls.append(int(indiv_scenes[0]))  # This appends the song ID
        music_time_start = indiv_scenes[1]
        music_time_end = indiv_scenes[2]
        inst_list = discover_instances(music_time_start, music_time_end)
        indiv_func_ls.append(inst_list[0])
        indiv_func_ls.append(inst_list[1])
        indiv_func_ls.append(inst_list[2])
        functional_ls.append(indiv_func_ls)
    return functional_ls  # [[ID, length, instances, remainder],....]


'''
The function below is for the creation of music 
'''
def making_music(blank_song, final_song_ls):
    for song_m in final_song_ls:
        for i in range(int(song_m[2])):
            if i == int(song_m[2]) - 1:
                input_song = AudioSegment.from_mp3(file_path_music + str(int(song_m[0])) + '.mp3')
                start_time = 15000
                end_time = start_time + song_m[3] * seconds_to_ms
                extract = input_song[start_time:end_time]
                softer = extract - 1
                faded_song = softer.fade_in(5000).fade_out(5000)
                blank_song += faded_song
            else:
                input_song = AudioSegment.from_mp3(file_path_music + str(int(song_m[0])) + '.mp3')
                start_time = 15000
                end_time = 44150
                extract = input_song[start_time:end_time]
                softer = extract - 1
                faded_song = softer.fade_in(5000).fade_out(5000)
                blank_song += faded_song
            # print(blank_song.duration_seconds)
    blank_song.export(combined_audio_file, format="mp3")

'''
VIDEO VIDEO VIDEO VIDEO VIDEO
'''
'''
Get all the emotion data into a list [[time1,V1,A1],[time2,V2,A2],...,[]]
'''
emo = step_video.extract_info(csv_emotion)
# print(emo)
# print(len(emo))  # This prints 1803 for the gladiator dataset
emotion.close()

'''
get the steps based on a certain time length
'''
print("This is the part where VA is average for X seconds")
steps = step_video.step_through(emo, 10)
print("The list containing fixed interval steps")
print(steps)
print("The total number of steps in the video")
print(len(steps))

'''
combine the steps that are close together
'''
print("This combines the steps that are deemed close to each other based as a percentage of max diff")
processed = step_video.process_steps(steps, emo, 0.1)
print("This is the processed list after merging")
print(processed)
print("This is the length of the processed list")
print(len(processed))

'''
plotting the 2D segments
'''
#  step_video.plot_2d_segments(processed)

'''
MUSIC MUSIC MUSIC MUSIC MUSIC
'''
ls_arousal = equations_music.full_info(csv_arousal)
ls_valence = equations_music.full_info(csv_valence)
# print(len(ls_arousal))  # 1802
# print(len(ls_arousal[0]))  # print the length of the first list. Output: 61 since it includes song_id
# print(ls_arousal[0]) # print the first list
#  print(len(ls_valence))  # 1802
# print(ls_valence)
arousal.close()
valence.close()

'''
plot the graph of valence/arousal against time
'''
# equations_music.plot_val_arou(ls_valence,ls_arousal, 485)

'''
computing the gradient and the intercept for each list
'''
ls_arousal_full = equations_music.best_fit_slope_and_intercept(ls_arousal)  # [[ID, Grad, Intercept],[ID, G, I],...]
ls_valence_full = equations_music.best_fit_slope_and_intercept(ls_valence)
# # print(ls_arousal_full)
# # print(ls_valence_full)

'''
computing the r^2 value
'''
ls_arousal_scored = equations_music.coefficient_of_determination(ls_arousal_full)
# #  print(ls_arousal_scored[0])
# #  print(ls_arousal_scored[1])
ls_valence_scored = equations_music.coefficient_of_determination(ls_valence_full)
# #  print(ls_valence_scored[0])

'''
filter out the values that dont meet the r requirement and print the length of the resulting list
'''
filtered_ls = equations_music.filter_by_r(ls_valence_scored, ls_arousal_scored, 0.8) # [[ID1, GV1, IV1, GA1, IA1],..]
print(filtered_ls)
print(len(filtered_ls))

'''
Filter by gradient
'''
final_music_ls = equations_music.filter_by_grad(filtered_ls, 0.004)
print(final_music_ls)
print("The number of available musics are")
print(len(final_music_ls))

'''
VIDEO MUSIC MATCHING
'''
matched_list = match_vid_music(processed, final_music_ls)
print(matched_list)

'''
SCENE SCENE SCENE
'''
scene_list = scene_detection.scene_info(csv_scenes)
print("This is a list of scenes with their starting times")
print(scene_list)
# print(len(scene_list))  # Should print 162
scene.close()

'''
getting the final ready to process music
'''

scene_va = music_start_end(matched_list,scene_list)
to_process = setting_the_music(scene_va)

print("This is the music that will be sent to the next algo")
print(to_process)
print(len(to_process))

'''
making the music
'''
making_music(combined_music, to_process)

'''
Combine the music with the movie
'''
video = mp.VideoFileClip(movie_to_overlay_music)
video.write_videofile(video_output_file, codec="libx264", audio=combined_audio_file)
