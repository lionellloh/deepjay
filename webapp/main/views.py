from django.shortcuts import render
from django.http import HttpResponse, JsonResponse

from celery import shared_task
from celery_progress.backend import ProgressRecorder
import time

import subprocess

from .inference import InferPlacesMLP
from .music import matching

from .music import scene_detection

@shared_task(bind=True)
def my_task(self, video):
    progress_recorder = ProgressRecorder(self)
    InferPlacesMLP.infer_video(video, progress_recorder)
    return 'done'

@shared_task(bind=True)
def my_task2(self, video):
    progress_recorder = ProgressRecorder(self)
    scene_detection.detect_scene(video, progress_recorder)
    return 'done'

@shared_task(bind=True)
def my_task3(self, video):
    progress_recorder = ProgressRecorder(self)
    matching.match_music(video, progress_recorder)
    return 'done'

def index(request):
    return render(request, 'main/display_progress.html')

def start(request, video):
    result = my_task.delay(video)
    result2 = my_task2.delay(video)
    result3 = my_task3.delay(video)

    return JsonResponse({'task_id': result.task_id, 'task_id2': result2.task_id, 'task_id3': result3.task_id })

