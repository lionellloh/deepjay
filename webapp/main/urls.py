from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('start/<str:video>', views.start, name='start'),
]
