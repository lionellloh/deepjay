from __future__ import print_function
import os

import scenedetect
from scenedetect.video_manager import VideoManager
from scenedetect.scene_manager import SceneManager
from scenedetect.frame_timecode import FrameTimecode
from scenedetect.stats_manager import StatsManager
from scenedetect.detectors import ContentDetector

import time

def detect_scene(video_path, progress_recorder=None):

    # Create a video_manager point to video file testvideo.mp4. Note that multiple
    # videos can be appended by simply specifying more file paths in the list
    # passed to the VideoManager constructor. Note that appending multiple videos
    # requires that they all have the same frame size, and optionally, framerate.
    video_manager = VideoManager([video_path])
    stats_manager = StatsManager()
    scene_manager = SceneManager(stats_manager)
    # Add ContentDetector algorithm (constructor takes detector options like threshold).
    scene_manager.add_detector(ContentDetector())
    base_timecode = video_manager.get_base_timecode()

    try:
        start_time = base_timecode     # 00:00:00.667
        # Set video_manager duration to read frames from 00:00:00 to 00:00:20.
        video_manager.set_duration(start_time=start_time)

        # Set downscale factor to improve processing speed (no args means default).
        video_manager.set_downscale_factor()

        # Start video_manager.
        video_manager.start()

        progress_recorder.set_progress(1, 3)
        time.sleep(1)

        # Perform scene detection on video_manager.
        scene_manager.detect_scenes(frame_source=video_manager)

        # Obtain list of detected scenes.
        scene_list = scene_manager.get_scene_list(base_timecode)
        # Like FrameTimecodes, each scene in the scene_list can be sorted if the
        # list of scenes becomes unsorted.

        progress_recorder.set_progress(2, 3)
        time.sleep(2)

        # Scene Number,Start Frame,Start Timecode,Start Time (seconds),End Frame,End Timecode,End Time (seconds),Length (frames),Length (timecode),Length (seconds)
        scene_number = 0

        print('List of scenes obtained:')
        csv_filename = video_path.split(".")[0] + "-Scenes.csv"
        with open(csv_filename, "w") as wf:
            for i, scene in enumerate(scene_list):
                row = "{},{},{},{},{},{},{},{},{},{}".format(i+1,
                    scene[0].frame_num,
                    scene[0].get_timecode(),
                    scene[0].get_seconds(),
                    scene[1].frame_num,
                    scene[1].get_timecode(),
                    scene[1].get_seconds(),
                    scene[1].frame_num - scene[0].frame_num,
                    (scene[1] - scene[0]).get_timecode(),
                    (scene[1] - scene[0]).get_seconds())

                wf.write(row + "\n")

        progress_recorder.set_progress(3, 3)

    finally:
        video_manager.release()

if __name__ == "__main__":
    main()


# import csv

# '''
# uncomment the code below for testing  
# '''
# # file_path_scene = "GLA-Scenes_limit.csv"
# #
# # scene = open(file_path_scene)
# # csv_scenes = csv.reader(scene)


def scene_info(file):
    #  row 0 is something that starts with 'Timecode List:'
    #  row 1 is the column headers, starts with 'Scene Number'
    #  row 3 to 164 are the scenes. total number of scenes is 162
    scene_start_time_ls = []
    for row in file:
        if row[0] == 'Timecode List:' or row[0] == 'Scene Number':
            pass  # skips these rows (Not needed)
        else:
            scene_start_time_ls.append(float(row[3]))  # For each song, there are 60 ratings
    return scene_start_time_ls  # the final list is the list of the start times of each scene

# '''
# Uncomment the code below to test the function
# '''
# # scene_list = scene_info(csv_scenes)
# # print(scene_list)
# # print(len(scene_list))  # Should print 162
# # scene.close()
