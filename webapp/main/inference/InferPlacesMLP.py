# coding: utf-8


import torch
from torch import nn
from torch.autograd import Variable
import torch.nn.functional as F
from torchvision import transforms as trn

from .SceneModel import SceneModel

import pandas as pd
import numpy as np
from PIL import Image
import cv2
import os
import sys

import matplotlib.pyplot as plt
from tqdm import tqdm

# Hack to fix some weird PyTorch 0.3 error (not required in 0.4)
import torch._utils
try:
    torch._utils._rebuild_tensor_v2
except AttributeError:
    def _rebuild_tensor_v2(storage, storage_offset, size, stride, requires_grad, backward_hooks):
        tensor = torch._utils._rebuild_tensor(storage, storage_offset, size, stride)
        tensor.requires_grad = requires_grad
        tensor._backward_hooks = backward_hooks
        return tensor
    torch._utils._rebuild_tensor_v2 = _rebuild_tensor_v2

def infer_video(filename, progress_recorder=None):
    np.random.seed(1337)
    torch.manual_seed(1337)

    gpu = -1

    # TODO: Change to use docopt or something
    video_path = filename #"256.mp4"
    output_dir = "/Users/kohjingyu/Documents/MachineLearning/QHBIC/tsinghua-team/running_script/inference/"

    def load_video(path):
        print("Loading data...", flush=True)

        video_frames = []
        video = cv2.VideoCapture(path)
        success, image = video.read()

        fps = int(video.get(cv2.CAP_PROP_FPS))
        count = 0

        while success:
            if count % fps == 0: # Take a frame every second
                video_frames.append(Image.fromarray(image)) # Add to list as a PIL Image

            success, image = video.read() # Continue
            count += 1

        print("Preprocessing video frames...", flush=True)
        data = []
        for frame in video_frames:
            # Center crop and perform mean subtraction
            centre_crop = trn.Compose([
            trn.Scale(256),
            trn.CenterCrop(224),
            trn.ToTensor(),
            trn.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ])

            cropped = centre_crop(frame)
            data.append(np.array(cropped))
        
        data_arr = np.array(data, dtype=np.float32)
        return data_arr

    def infer(data):
        abs_err = [0, 0]
        predictions = [[], []]
        labels = [[], []]

        print("Running inference for video...", flush=True)

        for i in range(len(data)):
            x = Variable(torch.from_numpy(data[i]).unsqueeze(0))
            
            if gpu != -1:
                x = x.cuda(gpu)
            
            pred = model(x)

            predictions[0].append(pred.data[0][0])
            predictions[1].append(pred.data[0][1])

            # TODO: Make it less hacky
            if progress_recorder:
                progress_recorder.set_progress(i + 1, len(data))

        return predictions

    # LOAD DATA
    data = load_video(video_path)

    model = SceneModel(gpu)
    if gpu != -1:
        model = model.cuda(gpu)

    # Testing
    best_state_dict = torch.load(output_dir + 'best_model.pth', map_location=lambda storage, loc: storage)
    model.load_state_dict(best_state_dict)

    predictions = infer(data)

    print("Done!")

    # plt.figure(figsize=(20, 5))
    # plt.subplot(121)
    # plt.plot(predictions[0])
    # plt.title("Valence predictions")
    # plt.subplot(122)
    # plt.plot(predictions[1])
    # plt.title("Arousal predictions")

    # plt.savefig("results.png")

    import pandas as pd
    pred_arr = np.array(predictions).transpose()
    df = pd.DataFrame(pred_arr, columns=["valence", "arousal"])

    video_filename = video_path.split('.')[0]
    df.to_csv("{}.csv".format(video_filename), header=False)

if __name__ == "__main__":
    infer_video(sys.argv[1])
