import matplotlib.pyplot as plt
import csv
from statistics import mean
import random


# file_path = "..\\COGNIMUSE Data\\GLA.csv"  # Edit this accordingly
#
# emotion = open(file_path)
# csv_emotion = csv.reader(emotion)  # note that for gladiator, there are 1802 data points


def extract_info(file):
    full_ls = []
    for row in file:
        indiv_list = []
        for entry in row:
            indiv_list.append(round(float(entry), 6))
        full_ls.append(indiv_list)
    return full_ls  # [[0,v1,a1][1,v2,a2],...,[t,v,a]]


def step_through(orig_raw_list, inter=10):
    full_ls = []
    for i in range(0, len(orig_raw_list) - len(orig_raw_list) % inter, inter):
        step_val = []
        step_arou = []
        indiv_ls = []
        start_time = i
        end_time = i + inter - 1  # VA Values that correspond to that time
        for j in range(inter):
            step_val.append(orig_raw_list[i+j][1])
            step_arou.append(orig_raw_list[i+j][2])
        average_val = round(mean(step_val), 3)
        average_arou = round(mean(step_arou), 3)
        indiv_ls.append(start_time)
        indiv_ls.append(end_time)
        indiv_ls.append(average_val)
        indiv_ls.append(average_arou)
        full_ls.append(indiv_ls)
    if len(orig_raw_list) % inter != 0:
        start_time = len(orig_raw_list) - len(orig_raw_list) % 10
        end_time = len(orig_raw_list) - 1  # VA values that corresponds to that time
        step_val = []
        step_arou = []
        indiv_ls = []
        for k in range(len(orig_raw_list) - len(orig_raw_list) % 10, len(orig_raw_list)):
            step_val.append(orig_raw_list[k][1])
            step_arou.append(orig_raw_list[k][2])
        average_val = round(mean(step_val), 3)
        average_arou = round(mean(step_arou), 3)
        indiv_ls.append(start_time)
        indiv_ls.append(end_time)
        indiv_ls.append(average_val)
        indiv_ls.append(average_arou)
        full_ls.append(indiv_ls)
    return full_ls  # [[start, end, ave_val, ave_aro], [start2, end2, ave_val, ave_arou],..[...]]


def create_shorter(raw_list, desired_video_len):
    num_data_points = len(raw_list)
    # randint(low_inclusive, high_not_inclusive)
    start_point = random.randint(0, num_data_points - desired_video_len + 1)  # the highest value is the v at index2 minus 1
    end_point = start_point + desired_video_len - 1
    indiv_short = []
    shortened_ls = []
    counter = 0
    scale = 1000
    time_interval = 1  # 2/desired_video_len
    for i in range(start_point, end_point + 1):
        indiv_short.append(counter/scale)
        indiv_short.append(raw_list[i][1])
        indiv_short.append(raw_list[i][2])
        shortened_ls.append(indiv_short)
        indiv_short = []
        counter += time_interval
    return shortened_ls, start_point


def find_diff(raw_list, threshold=0.1):
    val_ls = []
    arou_ls = []
    for i in raw_list:
        val_ls.append(i[1])
        arou_ls.append(i[2])
    max_val = max(val_ls)
    min_val = min(val_ls)
    max_arou = max(arou_ls)
    min_arou = min(arou_ls)
    tolerable_val_diff = abs((max_val - min_val)) * threshold
    tolerable_arou_diff = abs((max_arou - min_arou)) * threshold
    return tolerable_val_diff, tolerable_arou_diff


def process_steps(stepper_ls, raw_list, thresh=0.1):
    val_thresh, arou_thresh = find_diff(raw_list, thresh)
    print(val_thresh)
    print(arou_thresh)
    final_ls = []
    for i in range(len(stepper_ls)):
        # Check if the value is already in the previous lists.
        # Use the start time of the next entry and the end of last list
        if final_ls == []:  # Make sure that the current value is not in a list
            indiv_ls = []
            current_val = stepper_ls[i][2]
            current_arou = stepper_ls[i][3]
            start_time = stepper_ls[i][0]
            indiv_ls.append(start_time)
            indiv_ls.append(start_time)  # just to store some value, this will be replaced with the end time
            indiv_ls.append(current_val)
            indiv_ls.append(current_arou)
            if i >= len(stepper_ls) - 1:
                indiv_ls[1] = stepper_ls[i][1]
                final_ls.append(indiv_ls)
            else:
                for j in range(i + 1, len(stepper_ls)):
                    next_val = stepper_ls[j][2]
                    next_arou = stepper_ls[j][3]
                    if abs(current_val - next_val) < val_thresh and abs(current_arou - next_arou) < arou_thresh:
                        if j == len(stepper_ls) - 1:
                            indiv_ls[1] = stepper_ls[j][1]
                            final_ls.append(indiv_ls)
                            indiv_ls = []
                            break
                        else:
                            pass
                    elif i == j:
                        indiv_ls[1] = stepper_ls[i][1]
                        final_ls.append(indiv_ls)
                        indiv_ls = []
                        break
                    else:
                        indiv_ls[1] = stepper_ls[j-1][1]
                        final_ls.append(indiv_ls)
                        indiv_ls = []
                        break
        elif final_ls[-1][1] < stepper_ls[i][0]:
            indiv_ls = []
            current_val = stepper_ls[i][2]
            current_arou = stepper_ls[i][3]
            start_time = stepper_ls[i][0]
            indiv_ls.append(start_time)
            indiv_ls.append(start_time)  # just to store some value, this will be replaced with the end time
            indiv_ls.append(current_val)
            indiv_ls.append(current_arou)
            if i >= len(stepper_ls) - 1:
                indiv_ls[1] = stepper_ls[i][1]
                final_ls.append(indiv_ls)
            else:
                for j in range(i + 1, len(stepper_ls)):
                    next_val = stepper_ls[j][2]
                    next_arou = stepper_ls[j][3]
                    if abs(current_val - next_val) < val_thresh and abs(current_arou - next_arou) < arou_thresh:
                        if j == len(stepper_ls) - 1:
                            indiv_ls[1] = stepper_ls[j][1]
                            final_ls.append(indiv_ls)
                            indiv_ls = []
                            break
                        else:
                            pass
                    elif i == j:
                        indiv_ls[1] = stepper_ls[i][1]
                        final_ls.append(indiv_ls)
                        indiv_ls = []
                        break
                    else:
                        indiv_ls[1] = stepper_ls[j - 1][1]
                        final_ls.append(indiv_ls)
                        indiv_ls = []
                        break
    return final_ls  # this is of the form [[start1, endX, val1, arou1],[startX+1, endY, endX+1, arouX+1],...[...]]


# plot the segments in 2D space
def plot_2d_segments(seg):
    cluster_num = len(seg)
    for cluster_id in range(cluster_num):
        time_ls = [seg[cluster_id][0]]
        time_ls.append(seg[cluster_id][1])
        val_ls = [seg[cluster_id][2]]
        val_ls.append(seg[cluster_id][2])
        arou_ls = [seg[cluster_id][3]]
        arou_ls.append(seg[cluster_id][3])
        val_plot = plt.plot(time_ls, val_ls, 'b-')
        arou_plot = plt.plot(time_ls, arou_ls, 'r-')
    plt.axis([seg[0][0], int(seg[-1][0]), -1, 1])
    plt.xlabel('time/seconds')
    plt.ylabel('Valence (blue)/Arousal (red)')
    plt.title('Graph of VA against time (Gla Ground Truth) Pre Merging, 10 sec interval')
    plt.grid(True)
    plt.show()


# '''
# Get all the emotion data into a list [[time1,V1,A1],[time2,V2,A2],...,[]]
# '''
# emo = extract_info(csv_emotion)
# # print(emo)
# # print(len(emo))  # This prints 1803 for the gladiator dataset
# emotion.close()
#
# '''
# get the steps based on a certain time length
# '''
# steps = step_through(emo, 10)
# print(steps)
# print(len(steps))
#
# '''
# combine the steps that are close together
# '''
# processed = process_steps(steps, emo)
# print(processed)
# print(len(processed))
#
# '''
# plotting the 2D segments
# '''
# plot_2d_segments(processed)
