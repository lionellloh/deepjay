 #!/bin/bash

video=$1

# Ignore warnings for Python scripts
python -W ignore  ./inference/InferPlacesMLP.py $video && scenedetect --input $video detect-content list-scenes -o ./ && python -W ignore matching.py $video

