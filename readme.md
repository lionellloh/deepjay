# Deep Jay


Deep Jay is a system that combined deep learning and machine learning to make an emotion-sensitive music matching system. 

Deep Jay first strives to predict the fluctuating emotions evoked from a video qualitatively. With the emotional values extracted, Deep Jay then stitches together the most emotionally resonant music from a huge database of songs and matches them precisely to the video. This produces a video that evokes stronger feelings in its audience.

## Setup

Deep Jay has been tested on Python 3. To install the necessary requirements, run the following bash command:

```
pip install -r requirements.txt
```

## Data

The Deep Jay inference models are trained on the COGNIMUSE dataset. For more information and to download them, visit their [official website](http://cognimuse.cs.ntua.gr). It is necessary to preprocessing the COGNIMUSE videos to be frames instead of video files. These should be extracted to the COGNIMUSE files to the `COGNIMUSE` subfolder.

For music matching, we make use of the [Music DEAM dataset](http://cvml.unige.ch/databases/DEAM/). Download and extract the files to the `step_music_video_match` subfolder.

## Training

To train our models on COGNIMUSE, navigate to the `COGNIMUSE` folder.Then, edit `train.py` to fit your hyperparameters. Afterwards, run the training script as follows:

```
python train.py
```

For GPU acceleration, edit the `gpu` variable in the training script.

### RNN Model

To run the RNN model, edit the `SceneRNN.py` hyperparameter to fit your needs. Then, run

```
python SceneRNN.py
```

## Inference

`InferPlacesMLP.py` performs inference using a model saved by our training script. To run it, edit the script to update the files to fit your needs. Then, run

```
python InferPlacesMLP.py
```

The output of the inference script is a .csv file containing the predicted valence and arousal value of each second of the video.

## Music Matching

Our music matching algorithm requires the .csv file of valence arousal values as predicted by the previous model. In order to run it, navigate to the `step_music_video_match` folder. Edit  `matching.py` to match your video and csv file paths. Then, run

```
python matching.py
```

The result will be a video saved to disk containing the new audio overlay.

## Web Application

We also provide a web application interface implemented in Django to allow for ease of use. To run this, navigate to the `demo_app` folder. It is required to install and start a [rabbitmq](https://www.rabbitmq.com) server.

After doing this, start a [Celery](https://github.com/celery/celery) server:

```
celery -A webapp worker -l info -P solo
```

Finally, start the web application:

```
python manage.py runserver
```

This starts a web server located at http://localhost:8000.


