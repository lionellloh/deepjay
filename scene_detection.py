import csv


'''
uncomment the code below for testing  
'''
# file_path_scene = "GLA-Scenes_limit.csv"
#
# scene = open(file_path_scene)
# csv_scenes = csv.reader(scene)


def scene_info(file):
    #  row 0 is something that starts with 'Timecode List:'
    #  row 1 is the column headers, starts with 'Scene Number'
    #  row 3 to 164 are the scenes. total number of scenes is 162
    scene_start_time_ls = []
    for row in file:
        if row[0] == 'Timecode List:' or row[0] == 'Scene Number':
            pass  # skips these rows (Not needed)
        else:
            scene_start_time_ls.append(float(row[3]))  # For each song, there are 60 ratings
    return scene_start_time_ls  # the final list is the list of the start times of each scene

'''
Uncomment the code below to test the function
'''
# scene_list = scene_info(csv_scenes)
# print(scene_list)
# print(len(scene_list))  # Should print 162
# scene.close()
